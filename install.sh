#!/bin/bash

function isRoot() {
	if [ "${EUID}" -ne 0 ]; then
		imprimir red "Error: Necesitas ejecutar este archivo como root"
		exit 1
	fi
}

function checkOS() {
	# Revisar Versión del sistema
	if [[ -e /etc/debian_version ]]; then
		source /etc/os-release
		OS="${ID}" # debian o ubuntu
		if [[ ${ID} == "debian" || ${ID} == "raspbian" ]]; then
			if [[ ${VERSION_ID} -ne 10 ]]; then
				imprimir red "Error: Tu version de Debian (${VERSION_ID}) no es soportada, usa Debian 10 por lo menos"
				exit 1
			fi
		fi
	elif [[ -e /etc/fedora-release ]]; then
		source /etc/os-release
		OS="${ID}"
	elif [[ -e /etc/centos-release ]]; then
		source /etc/os-release
		OS=centos
	elif [[ -e /etc/arch-release ]]; then
		OS=arch
	else
		imprimir red "Error: No se podrá ejecutar el script ya que no fue posible identificar tu sistema operativo compatible"
		exit 1
	fi
}

# imprimir en color el texto
function imprimir() {
    if [ $1 = "red" ];
    then
        red='\033[0;31m'
        noc='\033[0m'
        echo -e ${red} $2 ${noc}
    fi
    
    if [ $1 = "blue" ];
    then
        red='\033[0;34m'
        noc='\033[0m'
        echo -e ${red} $2 ${noc}
    fi
    
    if [ $1 = "yellow" ];
    then
        red='\033[0;33m'
        noc='\033[0m'
        echo -e ${red} $2 ${noc}
    fi
    
    if [ $1 = "green" ];
    then
        red='\033[0;32m'
        noc='\033[0m'
        echo -e ${red} $2 ${noc}
    fi
    
    if [ $1 = "cyan" ];
    then
        red='\033[0;36m'
        noc='\033[0m'
        echo -e ${red} $2 ${noc}
    fi
    
    if [ $1 = "magenta" ];
    then
        red='\033[0;35m'
        noc='\033[0m'
        echo -e ${red} $2 ${noc}
    fi
}

function initialCheck() {
	isRoot
	ceckVirt
	checkOS
}

function runClean() {
    sudo apt update
    sudo apt remove --purge scratch2 minecraft-pi wolfram-engine sonic-pi dillo libreoffice libreoffice-avmedia-backend-gstreamer libreoffice-base libreoffice-base-core libreoffice-base-drivers libreoffice-calc libreoffice-common libreoffice-core libreoffice-draw libreoffice-gtk libreoffice-gtk2 libreoffice-impress libreoffice-java-common libreoffice-math libreoffice-pi libreoffice-report-builder-bin libreoffice-sdbc-hsqldb libreoffice-style-galaxy libreoffice-systray libreoffice-writer squeak-vm squeak-plugins-scratch geany

    imprimir yellow "Warning: Quitando paquetes innecesarios"
    imprimir yellow "Warning:  Tamaño antes de eliminar"
    df -h

    sudo apt-get remove --purge libreoffice* -y
    sudo apt-get remove --purge wolfram-engine -y
    sudo apt-get remove -—purge chromium-browser -y
    sudo apt-get remove --purge scratch2 -y
    sudo apt-get remove --purge minecraft-pi  -y
    sudo apt-get remove --purge sonic-pi  -y
    sudo apt-get remove --purge dillo -y
    sudo apt-get remove --purge gpicview -y
    sudo apt-get remove --purge penguinspuzzle -y
    sudo apt-get remove --purge oracle-java8-jdk -y
    sudo apt-get remove --purge openjdk-7-jre -y
    sudo apt-get remove --purge oracle-java7-jdk -y 
    sudo apt-get remove --purge openjdk-8-jre -y

    sudo apt-get clean
    sudo apt-get autoremove -y

    imprimir blue  "Success: nuevo tamaño del disco"
    df -h
}

function installDocker() {
    if [[ ${OS} == 'debian' ]]; then
        apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common vim ntfs-3g
        curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
        sudo apt-key fingerprint 0EBFCD88
        echo "deb [arch=armhf] https://download.docker.com/linux/debian \
              $(lsb_release -cs) stable" |     sudo tee /etc/apt/sources.list.d/docker.list
        sudo apt-get update && sudo apt-get install -y --no-install-recommends docker-ce docker-compose
        sudo usermod -a -G docker pi
    fi
}

initialCheck
runClean

